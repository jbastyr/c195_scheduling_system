package Constant;

public enum PatternConstant {
    DATE_FORMAT("M/d/y"),
    TIME_FORMAT("H:mm"),
    DATE_TIME_FORMAT("M/d/y H:mm");

    private String pattern;

    PatternConstant(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
