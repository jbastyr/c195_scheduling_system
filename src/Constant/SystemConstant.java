package Constant;

public enum SystemConstant {
    // every day 9a-5p, including weekends. I decided.
    BUSINESS_START("9:00"),
    BUSINESS_END("18:00");


    private String value;

    SystemConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
