package Constant;

public enum PathConstant {
    LOGIN_AUDIT_PATH("C:/temp/login.txt");

    private String path;

    PathConstant(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
