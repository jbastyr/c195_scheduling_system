package Exception;

public class ValidateException extends Exception {

    private String header = "";
    private String message = "";

    public ValidateException() {
        super();
    }

    public ValidateException(String header, String message) {
        super();
        this.header = header;
        this.message = message;
    }

    public String getHeader() {
        return header;
    }

    public String getMessage() {
        return message;
    }

}