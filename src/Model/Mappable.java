package Model;

import java.sql.ResultSet;

public interface Mappable {
    Mappable mapResultSet(ResultSet rs);
}
