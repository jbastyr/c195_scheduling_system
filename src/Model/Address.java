package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Address implements Mappable {
    private IntegerProperty addressId;
    private StringProperty address;
    private StringProperty address2;
    private IntegerProperty cityId;
    private StringProperty postalCode;
    private StringProperty phone;
    private LocalDateTime createDate;
    private StringProperty createdBy;
    private LocalDateTime lastUpdate;
    private StringProperty lastUpdateBy;

    public Address() {
        this.addressId = new SimpleIntegerProperty();
        this.address = new SimpleStringProperty();
        this.address2 = new SimpleStringProperty();
        this.cityId = new SimpleIntegerProperty();
        this.postalCode = new SimpleStringProperty();
        this.phone = new SimpleStringProperty();
        this.createDate = LocalDateTime.now();
        this.createdBy = new SimpleStringProperty();
        this.lastUpdate = LocalDateTime.now();
        this.lastUpdateBy = new SimpleStringProperty();
    }

    public int getAddressId() {
        return addressId.get();
    }

    public void setAddressId(int addressId) {
        this.addressId.set(addressId);
    }

    public IntegerProperty addressIdProperty() {
        return addressId;
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public StringProperty addressProperty() {
        return address;
    }

    public String getAddress2() {
        return address2.get();
    }

    public void setAddress2(String address2) {
        this.address2.set(address2);
    }

    public StringProperty address2Property() {
        return address2;
    }

    public int getCityId() {
        return cityId.get();
    }

    public void setCityId(int cityId) {
        this.cityId.set(cityId);
    }

    public IntegerProperty cityIdProperty() {
        return cityId;
    }

    public String getPostalCode() {
        return postalCode.get();
    }

    public void setPostalCode(String postalCode) {
        this.postalCode.set(postalCode);
    }

    public StringProperty postalCodeProperty() {
        return postalCode;
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy.get();
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy.set(createdBy);
    }

    public StringProperty createdByProperty() {
        return createdBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy.get();
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    public StringProperty lastUpdateByProperty() {
        return lastUpdateBy;
    }

    @Override
    public Address mapResultSet(ResultSet rs) {
        // change this to lambda?
        try {
            setAddressId(rs.getInt("addressId"));
            setAddress(rs.getString("address"));
            setAddress2(rs.getString("address2"));
            setCityId(rs.getInt("cityId"));
            setPostalCode(rs.getString("postalCode"));
            setPhone(rs.getString("phone"));
            setCreateDate(rs.getTimestamp("createDate").toLocalDateTime());
            setCreatedBy(rs.getString("createdBy"));
            setLastUpdate(rs.getTimestamp("lastUpdate").toLocalDateTime());
            setLastUpdateBy(rs.getString("lastUpdateBy"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public String toString() {
        return getAddress() + " " + getAddress2();
    }

    // gross but ok
    public boolean equals(Object o) {

        if (o instanceof Address) {
            Address address = (Address) o;

            return getAddressId() == address.getAddressId() &&
                    getAddress().equals(address.getAddress()) &&
                    getAddress2().equals(address.getAddress2()) &&
                    getCityId() == address.getCityId() &&
                    getPostalCode().equals(address.getPostalCode()) &&
                    getCreateDate().equals(address.getCreateDate()) &&
                    getCreatedBy().equals(address.getCreatedBy()) &&
                    getLastUpdate().equals(address.getLastUpdate()) &&
                    getLastUpdateBy().equals(getLastUpdateBy());
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + getAddressId();
        result = 31 * result + getAddress().hashCode();
        result = 31 * result + getAddress2().hashCode();
        result = 31 * result + getCityId();
        result = 31 * result + getPostalCode().hashCode();
        result = 31 * result + getCreateDate().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getLastUpdate().hashCode();
        result = 31 * result + getLastUpdateBy().hashCode();
        return result;
    }
}
