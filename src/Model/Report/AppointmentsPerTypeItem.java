package Model.Report;

import Model.Mappable;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppointmentsPerTypeItem implements Mappable, ReportableItem {
    private StringProperty type;
    private IntegerProperty count;
    private ObservableList<String> columns;

    public AppointmentsPerTypeItem() {
        type = new SimpleStringProperty();
        count = new SimpleIntegerProperty();
        columns = new SimpleListProperty<>(FXCollections.observableArrayList());
        columns.addAll("type", "count");
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public StringProperty typeProperty() {
        return type;
    }

    public int getCount() {
        return count.get();
    }

    public void setCount(int count) {
        this.count.set(count);
    }

    public IntegerProperty countProperty() {
        return count;
    }

    @Override
    public AppointmentsPerTypeItem mapResultSet(ResultSet rs) {
        try {
            setType(rs.getString("type"));
            setCount(rs.getInt("count"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public ObservableList<String> getColumns() {
        return columns;
    }

}
