package Model.Report;

import Model.Mappable;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ConsultantScheduleItem implements Mappable, ReportableItem {
    private StringProperty title;
    private StringProperty description;
    private LocalDateTime start;
    private StringProperty type;
    private ObservableList<String> columns;

    public ConsultantScheduleItem() {
        title = new SimpleStringProperty();
        description = new SimpleStringProperty();
        start = LocalDateTime.now();
        type = new SimpleStringProperty();
        columns = new SimpleListProperty<>(FXCollections.observableArrayList());
        columns.addAll("title", "description", "start", "type");
    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public StringProperty typeProperty() {
        return type;
    }

    public ConsultantScheduleItem mapResultSet(ResultSet rs) {
        try {
            setTitle(rs.getString("title"));
            setDescription(rs.getString("description"));
            setStart(rs.getTimestamp("start").toLocalDateTime());
            setType(rs.getString("type"));
        } catch (SQLException e) {
            return null;
        }
        return this;
    }

    public ObservableList<String> getColumns() {
        return columns;
    }

    public void setColumns(ObservableList<String> columns) {
        this.columns = columns;
    }
}
