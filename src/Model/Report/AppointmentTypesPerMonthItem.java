package Model.Report;

import Model.Mappable;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppointmentTypesPerMonthItem implements Mappable, ReportableItem {
    private StringProperty month;
    private IntegerProperty count;
    private ObservableList<String> columns;

    public AppointmentTypesPerMonthItem() {
        month = new SimpleStringProperty();
        count = new SimpleIntegerProperty();
        columns = new SimpleListProperty<>(FXCollections.observableArrayList());
        columns.addAll("month", "count");
    }

    public String getMonth() {
        return month.get();
    }

    public void setMonth(String month) {
        this.month.set(month);
    }

    public StringProperty monthProperty() {
        return month;
    }

    public int getCount() {
        return count.get();
    }

    public void setCount(int count) {
        this.count.set(count);
    }

    public IntegerProperty countProperty() {
        return count;
    }

    @Override
    public AppointmentTypesPerMonthItem mapResultSet(ResultSet rs) {
        try {
            setMonth(rs.getString("month"));
            setCount(rs.getInt("count"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public ObservableList<String> getColumns() {
        return columns;
    }
}
