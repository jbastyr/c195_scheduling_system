package Model.Report;

import javafx.collections.ObservableList;

public interface ReportableItem {
    ObservableList<String> getColumns();
}
