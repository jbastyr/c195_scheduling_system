package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class City implements Mappable {
    private IntegerProperty cityId;
    private StringProperty city;
    private IntegerProperty countryId;
    private LocalDateTime createDate;
    private StringProperty createdBy;
    private LocalDateTime lastUpdate;
    private StringProperty lastUpdateBy;

    public City() {
        this.cityId = new SimpleIntegerProperty();
        this.city = new SimpleStringProperty();
        this.countryId = new SimpleIntegerProperty();
        this.createDate = LocalDateTime.now();
        this.createdBy = new SimpleStringProperty();
        this.lastUpdate = LocalDateTime.now();
        this.lastUpdateBy = new SimpleStringProperty();
    }

    public int getCityId() {
        return cityId.get();
    }

    public void setCityId(int cityId) {
        this.cityId.set(cityId);
    }

    public IntegerProperty cityIdProperty() {
        return cityId;
    }

    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public StringProperty cityProperty() {
        return city;
    }

    public int getCountryId() {
        return countryId.get();
    }

    public void setCountryId(int countryId) {
        this.countryId.set(countryId);
    }

    public IntegerProperty countryIdProperty() {
        return countryId;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy.get();
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy.set(createdBy);
    }

    public StringProperty createdByProperty() {
        return createdBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy.get();
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    public String getlastUpdateBy() {
        return lastUpdateBy.get();
    }

    public StringProperty lastUpdateByProperty() {
        return lastUpdateBy;
    }

    public void setlastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    @Override
    public City mapResultSet(ResultSet rs) {
        try {
            setCityId(rs.getInt("cityId"));
            setCity(rs.getString("city"));
            setCountryId(rs.getInt("countryId"));
            setCreateDate(rs.getTimestamp("createDate").toLocalDateTime());
            setCreatedBy(rs.getString("createdBy"));
            setLastUpdate(rs.getTimestamp("lastUpdate").toLocalDateTime());
            setLastUpdateBy(rs.getString("lastUpdateBy"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public String toString() {
        return getCity();
    }

    public boolean equals(Object o) {
        if (o instanceof City) {
            City city = (City) o;

            return getCityId() == city.getCityId() &&
                    getCity().equals(city.getCity()) &&
                    getCountryId() == city.getCountryId() &&
                    getCreateDate().equals(city.getCreateDate()) &&
                    getCreatedBy().equals(city.getCreatedBy()) &&
                    getLastUpdate().equals(city.getLastUpdate()) &&
                    getlastUpdateBy().equals(city.getLastUpdateBy());
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + getCityId();
        result = 31 * result + getCity().hashCode();
        result = 31 * result + getCountryId();
        result = 31 * result + getCreateDate().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getLastUpdate().hashCode();
        result = 31 * result + getLastUpdateBy().hashCode();
        return result;
    }
}
