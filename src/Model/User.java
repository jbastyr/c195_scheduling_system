package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class User implements Mappable {
    private IntegerProperty userId;
    private StringProperty userName;
    private StringProperty password;
    private IntegerProperty active;
    private LocalDateTime createDate;
    private StringProperty createBy;
    private LocalDateTime lastUpdate;
    private StringProperty lastUpdateBy;

    public User() {
        this.userId = new SimpleIntegerProperty();
        this.userName = new SimpleStringProperty();
        this.password = new SimpleStringProperty();
        this.active = new SimpleIntegerProperty();
        this.createBy = new SimpleStringProperty();
        this.createDate = LocalDateTime.now();
        this.lastUpdate = LocalDateTime.now();
        this.lastUpdateBy = new SimpleStringProperty();
    }

    public int getUserId() {
        return userId.get();
    }

    public void setUserId(int userId) {
        this.userId.set(userId);
    }

    public IntegerProperty userIdProperty() {
        return userId;
    }

    public String getUserName() {
        return userName.get();
    }

    public void setUserName(String userName) {
        this.userName.set(userName);
    }

    public StringProperty userNameProperty() {
        return userName;
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public int getActive() {
        return active.get();
    }

    public void setActive(int active) {
        this.active.set(active);
    }

    public IntegerProperty activeProperty() {
        return active;
    }

    public String getCreateBy() {
        return createBy.get();
    }

    public void setCreateBy(String createBy) {
        this.createBy.set(createBy);
    }

    public StringProperty createByProperty() {
        return createBy;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public StringProperty lastUpdateByProperty() {
        return lastUpdateBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy.get();
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    // functionally, this is a constructor, but will keep it as is to allow for potentially
    // existing object to be overwritten by result set
    public User mapResultSet(ResultSet rs) {
        try {
            setUserId(rs.getInt("userId"));
            setUserName(rs.getString("userName"));
            setPassword(rs.getString("password"));
            setActive(rs.getInt("active"));
            setCreateDate(rs.getTimestamp("createDate").toLocalDateTime());
            setCreateBy(rs.getString("createBy"));
            setLastUpdate(rs.getTimestamp("lastUpdate").toLocalDateTime());
            setLastUpdateBy(rs.getString("lastUpdateBy"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public String toString() {
        return getUserName();
    }

    public boolean equals(Object o) {
        if (o instanceof User) {
            User user = (User) o;

            return getUserId() == user.getUserId() &&
                    getUserName().equals(user.getUserName()) &&
                    getPassword().equals(user.getPassword()) &&
                    getActive() == user.getActive() &&
                    getCreateDate().equals(user.getCreateDate()) &&
                    getCreateBy().equals(user.getCreateBy()) &&
                    getLastUpdate().equals(user.getLastUpdate()) &&
                    getLastUpdateBy().equals(user.getLastUpdateBy());
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + getUserId();
        result = 31 * result + getUserName().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getActive();
        result = 31 * result + getCreateDate().hashCode();
        result = 31 * result + getCreateBy().hashCode();
        result = 31 * result + getLastUpdate().hashCode();
        result = 31 * result + getLastUpdateBy().hashCode();
        return result;
    }

}