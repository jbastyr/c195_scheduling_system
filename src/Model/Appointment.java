package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Appointment implements Mappable {
    private IntegerProperty appointmentId;
    private IntegerProperty customerId;
    private StringProperty title;
    private StringProperty description;
    private StringProperty location;
    private StringProperty contact;
    private StringProperty url;
    private LocalDateTime start;
    private LocalDateTime end;
    private LocalDateTime createDate;
    private StringProperty createdBy;
    private LocalDateTime lastUpdate;
    private StringProperty lastUpdateBy;
    private StringProperty type;
    private IntegerProperty userId;

    public Appointment() {
        this.appointmentId = new SimpleIntegerProperty();
        this.customerId = new SimpleIntegerProperty();
        this.title = new SimpleStringProperty();
        this.description = new SimpleStringProperty();
        this.location = new SimpleStringProperty();
        this.contact = new SimpleStringProperty();
        this.url = new SimpleStringProperty();
        this.start = LocalDateTime.now();
        this.end = LocalDateTime.now();
        this.createDate = LocalDateTime.now();
        this.createdBy = new SimpleStringProperty();
        this.lastUpdate = LocalDateTime.now();
        this.lastUpdateBy = new SimpleStringProperty();
        this.type = new SimpleStringProperty();
        this.userId = new SimpleIntegerProperty();
    }

    public int getAppointmentId() {
        return appointmentId.get();
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId.set(appointmentId);
    }

    public IntegerProperty appointmentIdProperty() {
        return appointmentId;
    }

    public int getCustomerId() {
        return customerId.get();
    }

    public void setCustomerId(int customerId) {
        this.customerId.set(customerId);
    }

    public IntegerProperty customerIdProperty() {
        return customerId;
    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public StringProperty titleProperty() {
        return title;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getLocation() {
        return location.get();
    }

    public void setLocation(String location) {
        this.location.set(location);
    }

    public StringProperty locationProperty() {
        return location;
    }

    public String getContact() {
        return contact.get();
    }

    public void setContact(String contact) {
        this.contact.set(contact);
    }

    public StringProperty contactProperty() {
        return contact;
    }

    public String getUrl() {
        return url.get();
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public StringProperty urlProperty() {
        return url;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy.get();
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy.set(createdBy);
    }

    public StringProperty createdByProperty() {
        return createdBy;
    }

    public String getlastUpdateBy() {
        return lastUpdateBy.get();
    }

    public StringProperty lastUpdateByProperty() {
        return lastUpdateBy;
    }

    public void setlastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy.get();
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    public String getType() {
        return type.get();
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public StringProperty typeProperty() {
        return type;
    }

    public int getUserId() {
        return userId.get();
    }

    public void setUserId(int userId) {
        this.userId.set(userId);
    }

    public IntegerProperty userIdProperty() {
        return userId;
    }

    @Override
    public Appointment mapResultSet(ResultSet rs) {
        try {
            setAppointmentId(rs.getInt("appointmentId"));
            setCustomerId(rs.getInt("customerId"));
            setTitle(rs.getString("title"));
            setDescription(rs.getString("description"));
            setLocation(rs.getString("location"));
            setContact(rs.getString("contact"));
            setUrl(rs.getString("url"));
            setStart(rs.getTimestamp("start").toLocalDateTime());
            setEnd(rs.getTimestamp("end").toLocalDateTime());
            setCreateDate(rs.getTimestamp("createDate").toLocalDateTime());
            setCreatedBy(rs.getString("createdBy"));
            setLastUpdate(rs.getTimestamp("lastUpdate").toLocalDateTime());
            setLastUpdateBy(rs.getString("lastUpdateBy"));
            setType(rs.getString("type"));
            setUserId(rs.getInt("userId"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }
}
