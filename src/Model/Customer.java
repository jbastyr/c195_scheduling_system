package Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class Customer implements Mappable {

    private IntegerProperty customerId;
    private StringProperty customerName;
    private IntegerProperty addressId;
    private IntegerProperty active;
    private LocalDateTime createDate;
    private StringProperty createdBy;
    private LocalDateTime lastUpdate;
    private StringProperty lastUpdateBy;

    public Customer() {
        this.customerId = new SimpleIntegerProperty();
        this.customerName = new SimpleStringProperty();
        this.addressId = new SimpleIntegerProperty();
        this.active = new SimpleIntegerProperty();
        this.createDate = LocalDateTime.now();
        this.createdBy = new SimpleStringProperty();
        this.lastUpdate = LocalDateTime.now();
        this.lastUpdateBy = new SimpleStringProperty();
    }

    public int getCustomerId() {
        return customerId.get();
    }

    public void setCustomerId(int customerId) {
        this.customerId.set(customerId);
    }

    public IntegerProperty customerIdProperty() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName.get();
    }

    public void setCustomerName(String customerName) {
        this.customerName.set(customerName);
    }

    public StringProperty customerNameProperty() {
        return customerName;
    }

    public int getAddressId() {
        return addressId.get();
    }

    public void setAddressId(int addressId) {
        this.addressId.set(addressId);
    }

    public IntegerProperty addressIdProperty() {
        return addressId;
    }

    public int getActive() {
        return active.get();
    }

    public void setActive(int active) {
        this.active.set(active);
    }

    public IntegerProperty activeProperty() {
        return active;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getCreatedBy() {
        return createdBy.get();
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy.set(createdBy);
    }

    public StringProperty createdByProperty() {
        return createdBy;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy.get();
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy.set(lastUpdateBy);
    }

    public StringProperty lastUpdateByProperty() {
        return lastUpdateBy;
    }

    @Override
    public Customer mapResultSet(ResultSet rs) {
        try {
            setCustomerId(rs.getInt("customerId"));
            setCustomerName(rs.getString("customerName"));
            setAddressId(rs.getInt("addressId"));
            setActive(rs.getInt("active"));
            setCreateDate(rs.getTimestamp("createDate").toLocalDateTime());
            setCreatedBy(rs.getString("createdBy"));
            setLastUpdate(rs.getTimestamp("lastUpdate").toLocalDateTime());
            setLastUpdateBy(rs.getString("lastUpdateBy"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public String toString() {
        return getCustomerName();
    }

    public boolean equals(Object o) {
        if (o instanceof Customer) {
            Customer customer = (Customer) o;

            return getCustomerId() == customer.getCustomerId() &&
                    getCustomerName().equals(customer.getCustomerName()) &&
                    getAddressId() == customer.getAddressId() &&
                    getActive() == customer.getActive() &&
                    getCreateDate().equals(customer.getCreateDate()) &&
                    getCreatedBy().equals(customer.getCreatedBy()) &&
                    getLastUpdate().equals(customer.getLastUpdate()) &&
                    getLastUpdateBy().equals(customer.getLastUpdateBy());
        }
        return false;
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + getCustomerId();
        result = 31 * result + getCustomerName().hashCode();
        result = 31 * result + getAddressId();
        result = 31 * result + getActive();
        result = 31 * result + getCreateDate().hashCode();
        result = 31 * result + getCreatedBy().hashCode();
        result = 31 * result + getLastUpdate().hashCode();
        result = 31 * result + getLastUpdateBy().hashCode();
        return result;
    }

}
