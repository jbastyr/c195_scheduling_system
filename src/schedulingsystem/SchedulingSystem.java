package schedulingsystem;

import Service.AlertService;
import Service.DatabaseService;
import Service.SceneService;
import Service.TranslationService;
import javafx.application.Application;
import javafx.stage.Stage;

public class SchedulingSystem extends Application {
    private static AlertService alertService;
    private static SceneService sceneService;
    private static DatabaseService databaseService;
    private static TranslationService translationService;

    // preloadData removes everything from the db and adds back in sample data
    // comment it out if you do not want this to happen every start up
    public static void main(String[] args) {
        instantiateServices();
        databaseService.preloadData();
        launch(args);
    }

    private static void instantiateServices() {
        alertService = AlertService.getInstance();
        sceneService = SceneService.getInstance();
        databaseService = DatabaseService.getInstance();
        translationService = TranslationService.getInstance();
    }

    // LAMBDA
    // Lambda is used here in order to easily add the functionality for closing the stage
    // without creating a whole new class for it
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle(translationService.translate("Login.Scene.Title"));
        stage.setOnCloseRequest(event -> {
            if (!alertService.getAlertConfirmResponse(translationService.translate("Common.Exit.Header"),
                    translationService.translate("Common.Exit.Message"))) {
                event.consume();
            }
        });
        sceneService.launchScene(stage, getClass().getResource("/ViewController/View/Login.fxml"));
    }
}
