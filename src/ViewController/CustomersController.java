package ViewController;

import Model.Customer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;

public class CustomersController extends ServiceController implements Initializable {

    @FXML
    private BorderPane customerRoot;
    @FXML
    private TableView<Customer> tableCustomers;
    @FXML
    private TableColumn<Customer, Integer> columnCustomerID;
    @FXML
    private TableColumn<Customer, String> columnName;
    @FXML
    private Button buttonAddCustomer;
    @FXML
    private Button buttonEditCustomer;
    @FXML
    private Button buttonDeleteCustomer;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bindTable();
        fillTable();
    }

    @FXML
    void onClose(ActionEvent event) {
        sceneService.closeScene(customerRoot);
    }

    @FXML
    void onAddCustomer(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/ModifyCustomer.fxml"),
                translationService.translate("Customers.Add.Scene.Title"),
                true);
    }

    @FXML
    void onEditCustomer(ActionEvent event) {
        Customer customer = tableCustomers.getSelectionModel().getSelectedItem();
        if (customer == null) {
            return;
        }
        sceneService.launchScene(getClass().getResource("View/ModifyCustomer.fxml"),
                translationService.translate("Customers.Edit.Scene.Title"),
                true,
                customer);
    }

    @FXML
    void onDeleteCustomer(ActionEvent event) {
        Customer customer = tableCustomers.getSelectionModel().getSelectedItem();
        if (customer == null) {
            return;
        }
        try {
            customerService.deleteCustomer(customer.getCustomerId());
        } catch (SQLIntegrityConstraintViolationException e) {
            alertService.showAlertError(translationService.translate("SQL.Error.ForeignKeyConstraint.Header"),
                    translationService.translate("SQL.Error.ForeignKeyConstraint.Message"));
        } catch (SQLException e) {
            alertService.showAlertError(translationService.translate("SQL.Error.Generic.Header"),
                    translationService.translate("SQL.Error.Generic.Message"));
        }

    }

    private void bindTable() {
        tableBindingService.bindColumn(columnCustomerID, "customerId");
        tableBindingService.bindColumn(columnName, "customerName");
    }

    private void fillTable() {
        tableCustomers.setItems(customerService.getAllCustomers());
    }
}
