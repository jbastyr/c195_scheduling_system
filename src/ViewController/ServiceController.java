package ViewController;

import Service.*;

// dont instantiate this parent class but provide services to children. saves a lot of typing
// and provides all services to all controllers if they want it
abstract class ServiceController {
    //Utility
    AlertService alertService;
    DatabaseService databaseService;
    LoginService loginService;
    ReportService reportService;
    SceneService sceneService;
    SessionService sessionService;
    TableBindingService tableBindingService;
    TimeService timeService;
    TranslationService translationService;

    //Data
    AddressService addressService;
    AppointmentService appointmentService;
    CityService cityService;
    CustomerService customerService;
    UserService userService;

    ServiceController() {
        instantiateServices();
    }

    private void instantiateServices() {
        alertService = AlertService.getInstance();
        databaseService = DatabaseService.getInstance();
        loginService = LoginService.getInstance();
        reportService = ReportService.getInstance();
        sceneService = SceneService.getInstance();
        sessionService = SessionService.getInstance();
        tableBindingService = TableBindingService.getInstance();
        timeService = TimeService.getInstance();
        translationService = TranslationService.getInstance();

        addressService = AddressService.getInstance();
        appointmentService = AppointmentService.getInstance();
        customerService = CustomerService.getInstance();
        cityService = CityService.getInstance();
        userService = UserService.getInstance();
    }
}
