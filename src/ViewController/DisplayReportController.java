package ViewController;

import Model.Report.ReportableItem;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class DisplayReportController extends ServiceController implements DataAcceptor, Initializable {

    @FXML
    private BorderPane displayReportRoot;
    @FXML
    private Button buttonClose;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


    @FXML
    void onClose(ActionEvent event) {
        sceneService.closeScene(displayReportRoot);
    }

    @Override
    public void setData(Object data) {
        if (data instanceof ObservableList) {
            ObservableList list = (ObservableList) data;
            if (!list.isEmpty() && list.get(0) instanceof ReportableItem) {
                // this is kind of checked...
                ObservableList<ReportableItem> items = (ObservableList<ReportableItem>) list;

                displayReportRoot.centerProperty().set(reportService.tableViewFromList(items));
            } else {
                displayReportRoot.centerProperty().set(new Label(translationService.translate("Reports.Display.Scene.EmptyTable")));
            }
        } else {
            displayReportRoot.centerProperty().set(new Label(translationService.translate("Reports.Display.Scene.ErrorSettingData")));
        }
    }
}
