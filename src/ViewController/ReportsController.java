package ViewController;

import Converter.UserConverter;
import Model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ResourceBundle;

public class ReportsController extends ServiceController implements Initializable {
    @FXML
    private GridPane reportsRoot;
    @FXML
    private Button buttonAppointmentTypesPerMonth;
    @FXML
    private Button buttonConsultantSchedule;
    @FXML
    private ChoiceBox<User> choiceUser;
    @FXML
    private Button buttonAppointmentsPerType;
    @FXML
    private Button buttonClose;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setupChoiceBox();
    }

    @FXML
    void onAppointmentTypesPerMonth(ActionEvent event) {
        sceneService.launchScene(getClass().getResource(
                "View/DisplayReport.fxml"),
                String.format(
                        translationService.translate("Reports.Display.Scene.Title"), "Appointment Types per Month"
                ),
                true,
                reportService.appointmentTypesPerMonth());
    }

    @FXML
    void onConsultantSchedule(ActionEvent event) {
        if (choiceUser.getSelectionModel().getSelectedItem() == null) {
            alertService.showAlertError("No consultant selected",
                    "Select a consultant to show schedule");
            return;
        }
        User user = choiceUser.getSelectionModel().getSelectedItem();
        sceneService.launchScene(getClass().getResource(
                "View/DisplayReport.fxml"),
                String.format(
                        translationService.translate("Reports.Display.Scene.Title"), "Consultant Schedule"
                ),
                true,
                reportService.schedulePerConsultant(user));
    }

    @FXML
    void onAppointmentsPerType(ActionEvent event) {
        sceneService.launchScene(getClass().getResource(
                "View/DisplayReport.fxml"),
                String.format(
                        translationService.translate("Reports.Display.Scene.Title"), "Appointments Per Type"
                ),
                true,
                reportService.appointmentsPerType());
    }

    @FXML
    void onClose(ActionEvent event) {
        sceneService.closeScene(reportsRoot);
    }

    void setupChoiceBox() {
        choiceUser.setItems(userService.getAllUsers());
        choiceUser.setConverter(new UserConverter<User>());
    }
}
