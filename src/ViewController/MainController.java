package ViewController;

import Constant.DateConstant;
import Model.Appointment;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.util.Locale;
import java.util.ResourceBundle;

import static Constant.DateConstant.MONTH;
import static Constant.DateConstant.WEEK;

public class MainController extends ServiceController implements Initializable {


    private final long APPT_NOTIFY_TIME_MINUTES = 15;
    @FXML
    private BorderPane mainRoot;
    @FXML
    private Button buttonCustomers;
    @FXML
    private TableView<Appointment> tableAppointments;
    @FXML
    private TableColumn<Appointment, String> columnTitle;
    @FXML
    private TableColumn<Appointment, String> columnDescription;
    @FXML
    private TableColumn<Appointment, String> columnLocation;
    @FXML
    private TableColumn<Appointment, String> columnContact;
    @FXML
    private TableColumn<Appointment, String> columnURL;
    @FXML
    private TableColumn<Appointment, Timestamp> columnStart;
    @FXML
    private TableColumn<Appointment, Timestamp> columnEnd;
    @FXML
    private TableColumn<Appointment, String> columnType;
    @FXML
    private TableColumn<Appointment, Integer> columnUserId;
    @FXML
    private Button buttonNext;
    @FXML
    private Button buttonPrevious;
    @FXML
    private ToggleGroup viewByRadioGroup;
    @FXML
    private RadioButton radioViewByMonth;
    @FXML
    private RadioButton radioViewByWeek;
    @FXML
    private Button buttonAddAppointment;
    @FXML
    private Button buttonEditAppointment;
    @FXML
    private Button buttonDeleteAppointment;
    private DateConstant currentInterval;
    private int systemMonthPeriod;
    private int systemtWeekPeriod;
    private int currentPeriodOffset;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bindTable();
        fillTable();
        setupFilterData();
        checkForAppointmentSoon();
        filterTable(currentInterval, currentPeriodOffset);
    }

    @FXML
    void onCustomers(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/Customers.fxml"),
                translationService.translate("Customers.Scene.Title"),
                true);
    }

    @FXML
    void onReports(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/Reports.fxml"),
                translationService.translate("Reports.Scene.Title"),
                true);
    }

    @FXML
    void onShowLogins(ActionEvent event) {
        try {
            loginService.showLoginAuditFile();
        } catch (IOException e) {
            alertService.showAlertError(
                    translationService.translate("Common.Error.IO.Header"),
                    translationService.translate("Common.Error.IO.Message")
            );
        }
    }

    @FXML
    void onNext(ActionEvent event) {
        // set filter as +currentInterval
        filterTable(currentInterval, ++currentPeriodOffset);
    }

    @FXML
    void onPrevious(ActionEvent event) {
        filterTable(currentInterval, --currentPeriodOffset);
    }

    @FXML
    void onViewByMonth(ActionEvent event) {
        checkSelectedViewInterval();
    }

    @FXML
    void onViewByWeek(ActionEvent event) {
        checkSelectedViewInterval();
    }


    @FXML
    void onAddAppointment(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/ModifyAppointment.fxml"),
                translationService.translate("Appointment.Add.Scene.Title"),
                true);
        filterTable(currentInterval, currentPeriodOffset);
    }

    @FXML
    void onEditAppointment(ActionEvent event) {
        Appointment appointment = tableAppointments.getSelectionModel().getSelectedItem();
        if (appointment == null) {
            return;
        }
        sceneService.launchScene(getClass().getResource("View/ModifyAppointment.fxml"),
                translationService.translate("Appointment.Edit.Scene.Title"),
                true,
                appointment);
        filterTable(currentInterval, currentPeriodOffset);
    }

    @FXML
    void onDeleteAppointment(ActionEvent event) {
        Appointment appointment = tableAppointments.getSelectionModel().getSelectedItem();
        if (appointment == null) {
            return;
        }
        try {
            appointmentService.deleteAppointment(appointment.getAppointmentId());
        } catch (SQLException e) {
            alertService.showAlertError(translationService.translate("SQL.Error.Generic.Header"),
                    translationService.translate("SQL.Error.Generic.Message"));
        }
        filterTable(currentInterval, currentPeriodOffset);
    }

    private void bindTable() {
        tableBindingService.bindColumn(columnTitle, "title");
        tableBindingService.bindColumn(columnDescription, "description");
        tableBindingService.bindColumn(columnLocation, "location");
        tableBindingService.bindColumn(columnContact, "contact");
        tableBindingService.bindColumn(columnURL, "url");
        tableBindingService.bindColumn(columnStart, "start");
        tableBindingService.bindColumn(columnEnd, "end");
        tableBindingService.bindColumn(columnType, "type");
        tableBindingService.bindColumn(columnUserId, "userId");
    }

    private void fillTable() {
        filterTable(MONTH, LocalDateTime.now().getMonthValue());
    }

    // LAMBDA
    // lambda used here to filter the datatable to whichever kind of interval and page through it
    // with next and previous based on these values. Used because this is how filtered works and it
    // allows to only display items of the list for which the predicate is true
    private void filterTable(DateConstant interval, int periodOffset) {
        switch (interval) {
            case MONTH: {
                tableAppointments.setItems(
                        appointmentService.getAllAppointments().filtered(appointment ->
                                appointment.getStart().getMonthValue() == systemMonthPeriod + periodOffset
                        )
                );
            }
            break;
            case WEEK: {
                tableAppointments.setItems(
                        appointmentService.getAllAppointments().filtered(appointment ->
                                appointment.getStart().get(
                                        WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear()
                                ) == systemtWeekPeriod + periodOffset
                        )
                );
            }
            break;
        }
    }

    private void checkSelectedViewInterval() {
        // reset paging to default
        currentPeriodOffset = 0;
        if (radioViewByMonth.isSelected()) {
            currentInterval = MONTH;
            filterTable(currentInterval, currentPeriodOffset);
            buttonNext.setText("Next Month");
            buttonPrevious.setText("Previous Month");
        }
        if (radioViewByWeek.isSelected()) {
            currentInterval = WEEK;
            filterTable(currentInterval, currentPeriodOffset);
            buttonNext.setText("Next Week");
            buttonPrevious.setText("Previous Week");
        }
    }

    // default values for filter data, based on current date
    private void setupFilterData() {
        currentInterval = MONTH;
        currentPeriodOffset = 0;
        systemMonthPeriod = LocalDateTime.now().getMonthValue();
        systemtWeekPeriod = LocalDateTime.now().get(WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear());
    }

    // LAMBDA
    // Used here to filter and map through the elements to decide which to throw an alert for
    // Want to filter for current user's appointments and those starting within the next 15 minutes inclusive
    private void checkForAppointmentSoon() {
        appointmentService.getAllAppointments().filtered(appointment ->
                appointment.getUserId() == sessionService.getCurrentUser().getUserId() &&
                        LocalDateTime.now().until(appointment.getStart(), ChronoUnit.MINUTES) >= 0 &&
                        LocalDateTime.now().until(appointment.getStart(), ChronoUnit.MINUTES) <= APPT_NOTIFY_TIME_MINUTES
        ).forEach(appointment ->
                alertService.showAlertInfo(
                        translationService.translate("Main.Appointment.Soon.Header"),
                        String.format(translationService.translate("Main.Appointment.Soon.Message"), appointment.getTitle())
                )
        );
    }
}