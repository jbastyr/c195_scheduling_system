package ViewController;

import Converter.CityConverter;
import Exception.ValidateException;
import Model.Address;
import Model.City;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class ModifyAddressController extends ServiceController implements Initializable, DataAcceptor {
    @FXML
    private BorderPane modifyAddressRoot;
    @FXML
    private TextField fieldAddress;
    @FXML
    private TextField fieldAddress2;
    @FXML
    private ChoiceBox<City> choiceCity;
    @FXML
    private TextField fieldPostalCode;
    @FXML
    private TextField fieldPhone;

    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonReset;
    @FXML
    private Button buttonSave;

    private boolean isUpdate = false;
    private int storedId = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupChoiceboxes();
    }

    @FXML
    void onCancel(ActionEvent event) {
        sceneService.closeScene(modifyAddressRoot);
    }

    @FXML
    void onReset(ActionEvent event) {
        fieldAddress.setText("");
        fieldAddress2.setText("");
        choiceCity.getSelectionModel().clearSelection();
        fieldPostalCode.setText("");
        fieldPhone.setText("");
    }

    @FXML
    void onSave(ActionEvent event) {
        try {
            checkValidOptions();
        } catch (ValidateException e) {
            alertService.showAlertError(e.getHeader(), e.getMessage());
            return;
        }
        Address address = new Address();
        address.setAddress(fieldAddress.getText());
        address.setAddress2(fieldAddress2.getText());
        address.setCityId(choiceCity.getSelectionModel().getSelectedItem().getCityId());
        address.setPostalCode(fieldPostalCode.getText());
        address.setPhone(fieldPhone.getText());
        address.setCreatedBy(sessionService.getCurrentUser().getUserName());
        address.setLastUpdateBy(sessionService.getCurrentUser().getUserName());

        if (isUpdate) {
            address.setAddressId(storedId);
            addressService.updateAddress(address);
        } else {
            addressService.addAddress(address);
        }

        sceneService.closeScene(modifyAddressRoot);
    }

    private void checkValidOptions() throws ValidateException {
        if (fieldAddress.getText().isEmpty()) {
            throw throwForDataMissing("Address");
        }
        if (choiceCity.getSelectionModel().getSelectedItem() == null) {
            throw throwForDataMissing("City");
        }
        if (fieldPostalCode.getText().isEmpty()) {
            throw throwForDataMissing("Postal Code");
        }
        if (fieldPhone.getText().isEmpty()) {
            throw throwForDataMissing("Phone");
        }
    }

    private ValidateException throwForDataMissing(String badData) {
        return new ValidateException(
                translationService.translate("Common.Error.Data.Missing.Header"),
                String.format(translationService.translate("Common.Error.Data.Missing.Message"), badData)
        );
    }

    @Override
    public void setData(Object data) {
        if (data instanceof Address) {
            Address address = (Address) data;
            fieldAddress.setText(address.getAddress());
            fieldAddress2.setText(address.getAddress2());
            choiceCity.getSelectionModel().select(cityService.getCity(address.getCityId()));
            fieldPostalCode.setText(address.getPostalCode());
            fieldPhone.setText(address.getPhone());
            isUpdate = true;
            storedId = address.getAddressId();
        }
    }

    public void setupChoiceboxes() {
        choiceCity.setItems(cityService.getAllCities());
        choiceCity.setConverter(new CityConverter<City>());
    }
}
