package ViewController;

import Model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class LoginController extends ServiceController {

    @FXML
    private BorderPane loginRoot;
    @FXML
    private Label labelHeader;
    @FXML
    private PasswordField fieldPassword;
    @FXML
    private Button buttonLogin;
    @FXML
    private TextField fieldUsername;
    @FXML
    private Button buttonClear;

    public LoginController() {

    }

    @FXML
    void onLogin(ActionEvent event) {
        if (fieldUsername.getText().isEmpty()) {
            alertService.showAlertError(translationService.translate("Login.MissingUsername.Header"),
                    translationService.translate("Login.MissingUsername.Header"));
            return;
        }
        if (fieldPassword.getText().isEmpty()) {
            alertService.showAlertError(translationService.translate("Login.MissingPassword.Header"),
                    translationService.translate("Login.MissingPassword.Header"));
            return;
        }
        User user = loginService.login(fieldUsername.getText(), fieldPassword.getText());
        if (user != null) {
            sessionService.setCurrentUser(user);
            sceneService.launchScene(getClass().getResource("View/Main.fxml"),
                    translationService.translate("Main.Scene.Title"), false);
            sceneService.closeScene(loginRoot);
        } else {
            alertService.showAlertError(translationService.translate("Login.Failed.Header"),
                    translationService.translate("Login.Failed.Message"));
        }
    }

    @FXML
    void onClear(ActionEvent event) {
        fieldUsername.setText("");
        fieldPassword.setText("");
    }
}
