package ViewController;

import Model.Address;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ResourceBundle;

public class AddressesController extends ServiceController implements Initializable {

    @FXML
    private BorderPane addressesRoot;

    @FXML
    private TableView<Address> tableAddresses;
    @FXML
    private TableColumn<Address, Integer> columnAddressID;
    @FXML
    private TableColumn<Address, String> columnAddress;
    @FXML
    private TableColumn<Address, String> columnAddress2;
    @FXML
    private TableColumn<Address, Integer> columnCityID;
    @FXML
    private TableColumn<Address, String> columnPostalCode;
    @FXML
    private TableColumn<Address, String> columnPhone;

    @FXML
    private Button buttonAddAddress;
    @FXML
    private Button buttonEditAddress;
    @FXML
    private Button buttonDeleteAddress;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bindTable();
        fillTable();
    }

    @FXML
    void onClose(ActionEvent event) {
        sceneService.closeScene(addressesRoot);
    }

    @FXML
    void onAddAddress(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/ModifyAddress.fxml"),
                translationService.translate("Addresses.Add.Scene.Title"),
                true);
    }

    @FXML
    void onEditAddress(ActionEvent event) {
        Address address = tableAddresses.getSelectionModel().getSelectedItem();
        if (address == null) {
            return;
        }
        sceneService.launchScene(getClass().getResource("View/ModifyAddress.fxml"),
                translationService.translate("Addresses.Edit.Scene.Title"),
                true,
                address);

    }

    @FXML
    void onDeleteAddress(ActionEvent event) {
        Address address = tableAddresses.getSelectionModel().getSelectedItem();
        if (address == null) {
            return;
        }
        try {
            addressService.deleteAddress(address.getAddressId());
        } catch (SQLIntegrityConstraintViolationException e) {
            alertService.showAlertError(translationService.translate("SQL.Error.ForeignKeyConstraint.Header"),
                    translationService.translate("SQL.Error.ForeignKeyConstraint.Message"));
        } catch (SQLException e) {
            alertService.showAlertError(translationService.translate("SQL.Error.Generic.Header"),
                    translationService.translate("SQL.Error.Generic.Message"));
        }
    }


    private void bindTable() {
        tableBindingService.bindColumn(columnAddressID, "addressId");
        tableBindingService.bindColumn(columnAddress, "address");
        tableBindingService.bindColumn(columnAddress2, "address2");
        tableBindingService.bindColumn(columnCityID, "cityId");
        tableBindingService.bindColumn(columnPostalCode, "postalCode");
        tableBindingService.bindColumn(columnPhone, "phone");

    }

    private void fillTable() {
        tableAddresses.setItems(addressService.getAllAddresses());
    }

}
