package ViewController;

import Converter.AddressConverter;
import Exception.ValidateException;
import Model.Address;
import Model.Customer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class ModifyCustomerController extends ServiceController implements Initializable, DataAcceptor {
    @FXML
    private BorderPane modifyCustomerRoot;
    @FXML
    private TextField fieldName;
    @FXML
    private ChoiceBox<Address> choiceAddress;
    @FXML
    private Button buttonAddresses;
    @FXML
    private CheckBox checkboxActive;
    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonReset;
    @FXML
    private Button buttonSave;

    private boolean isUpdate = false;
    private int storedId = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setupChoiceboxes();
    }

    @FXML
    void onEditAddresses(ActionEvent event) {
        sceneService.launchScene(getClass().getResource("View/Addresses.fxml"),
                translationService.translate("Addresses.Scene.Title"),
                true);
    }

    @FXML
    void onCancel(ActionEvent event) {
        sceneService.closeScene(modifyCustomerRoot);
    }

    @FXML
    void onReset(ActionEvent event) {
        fieldName.setText("");
        choiceAddress.getSelectionModel().clearSelection();
        checkboxActive.setSelected(true);
    }

    @FXML
    void onSave(ActionEvent event) {
        try {
            checkValidOptions();
        } catch (ValidateException e) {
            alertService.showAlertError(e.getHeader(), e.getMessage());
            return;
        }

        Customer customer = new Customer();
        customer.setCustomerName(fieldName.getText());
        customer.setAddressId(choiceAddress.getSelectionModel().getSelectedItem().getAddressId());
        // no boolean type...
        customer.setActive(checkboxActive.isSelected() ? 1 : 0);
        customer.setCreatedBy(sessionService.getCurrentUser().getUserName());
        customer.setLastUpdateBy(sessionService.getCurrentUser().getUserName());

        if (isUpdate) {
            customer.setCustomerId(storedId);
            customerService.updateCustomer(customer);
        } else {
            customerService.addCustomer(customer);
        }

        sceneService.closeScene(modifyCustomerRoot);
    }

    private void checkValidOptions() throws ValidateException {
        if (fieldName.getText().isEmpty()) {
            throw throwForDataMissing("Name");
        }
        if (choiceAddress.getSelectionModel().getSelectedItem() == null) {
            throw throwForDataMissing("Address");
        }
    }

    private ValidateException throwForDataMissing(String badData) {
        return new ValidateException(
                translationService.translate("Common.Error.Data.Missing.Header"),
                String.format(translationService.translate("Common.Error.Data.Missing.Message"), badData)
        );
    }


    @Override
    public void setData(Object data) {
        if (data instanceof Customer) {
            Customer customer = (Customer) data;
            fieldName.setText(customer.getCustomerName());
            //this.choiceAddress.setValue(addressService.getAddress(customer.getAddressId()));
            choiceAddress.getSelectionModel().select(addressService.getAddress(customer.getAddressId()));
            // no boolean type...
            checkboxActive.setSelected(customer.getActive() == 1);

            isUpdate = true;
            storedId = customer.getCustomerId();
        }
    }

    private void setupChoiceboxes() {
        choiceAddress.setItems(addressService.getAllAddresses());
        choiceAddress.setConverter(new AddressConverter<Address>());
    }

}
