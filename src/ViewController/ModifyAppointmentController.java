package ViewController;

import Converter.CustomerConverter;
import Converter.UserConverter;
import Model.Appointment;
import Model.Customer;
import Model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import static Constant.PatternConstant.*;
import static Constant.SystemConstant.BUSINESS_END;
import static Constant.SystemConstant.BUSINESS_START;

public class ModifyAppointmentController extends ServiceController implements Initializable, DataAcceptor {


    @FXML
    private BorderPane modifyAppointmentRoot;
    @FXML
    private ChoiceBox<Customer> choiceCustomer;
    @FXML
    private TextField fieldTitle;
    @FXML
    private TextField fieldDescription;
    @FXML
    private TextField fieldLocation;
    @FXML
    private TextField fieldContact;
    @FXML
    private TextField fieldUrl;
    @FXML
    private DatePicker dateDate;
    @FXML
    private TextField fieldStart;
    @FXML
    private TextField fieldEnd;
    @FXML
    private TextField fieldType;
    @FXML
    private ChoiceBox<User> choiceUser;

    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonReset;
    @FXML
    private Button buttonSave;

    private boolean isUpdate = false;
    private int storedId = 0;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setupChoiceboxes();
    }

    @FXML
    void onCancel(ActionEvent event) {
        sceneService.closeScene(modifyAppointmentRoot);
    }

    @FXML
    void onReset(ActionEvent event) {
        choiceCustomer.getSelectionModel().clearSelection();
        fieldTitle.setText("");
        fieldDescription.setText("");
        fieldLocation.setText("");
        fieldContact.setText("");
        fieldUrl.setText("");
        dateDate.getEditor().clear();
        fieldStart.setText("");
        fieldEnd.setText("");
        fieldType.setText("");
        choiceUser.getSelectionModel().clearSelection();
    }

    @FXML
    void onSave(ActionEvent event) {
        if (!validOptions()) {
            return;
        }
        Appointment appointment = new Appointment();
        appointment.setCustomerId(choiceCustomer.getSelectionModel().getSelectedItem().getCustomerId());
        appointment.setTitle(fieldTitle.getText());
        appointment.setDescription(fieldDescription.getText());
        appointment.setLocation(fieldLocation.getText());
        appointment.setContact(fieldContact.getText());
        appointment.setUrl(fieldUrl.getText());
        appointment.setStart(LocalDateTime.parse(
                dateDate.getEditor().getText() + " " + fieldStart.getText(),
                DateTimeFormatter.ofPattern(DATE_TIME_FORMAT.getPattern())));
        appointment.setEnd(LocalDateTime.parse(
                dateDate.getEditor().getText() + " " + fieldEnd.getText(),
                DateTimeFormatter.ofPattern(DATE_TIME_FORMAT.getPattern())));
        appointment.setCreatedBy(sessionService.getCurrentUser().getUserName());
        appointment.setLastUpdateBy(sessionService.getCurrentUser().getUserName());
        appointment.setType(fieldType.getText());
        appointment.setUserId(choiceUser.getSelectionModel().getSelectedItem().getUserId());

        if (isUpdate) {
            appointment.setAppointmentId(storedId);
            appointmentService.updateAppointment(appointment);
        } else {
            appointmentService.addAppointmnet(appointment);
        }

        sceneService.closeScene(modifyAppointmentRoot);
    }

    // I hate this, ideally create this all in another class specifically for validation...
    private boolean validOptions() {
        return optionsSet() && timeParseValid() && timeBusinessValid() && timeOverlapValid();
    }


    private boolean optionsSet() {
        if (choiceCustomer.getSelectionModel().getSelectedItem() == null) {
            showDataMissingError("Customer");
            return false;
        }
        if (fieldTitle.getText().isEmpty()) {
            showDataMissingError("Title");
            return false;
        }
        if (fieldDescription.getText().isEmpty()) {
            showDataMissingError("Description");
            return false;
        }
        if (fieldLocation.getText().isEmpty()) {
            showDataMissingError("Location");
            return false;
        }
        if (fieldContact.getText().isEmpty()) {
            showDataMissingError("Contact");
            return false;
        }
        if (dateDate.getEditor().getText().isEmpty()) {
            showDataMissingError("Day");
            return false;
        }
        if (fieldStart.getText().isEmpty()) {
            showDataMissingError("Start");
            return false;
        }
        if (fieldEnd.getText().isEmpty()) {
            showDataMissingError("End");
            return false;
        }
        if (fieldType.getText().isEmpty()) {
            showDataMissingError("Type");
            return false;
        }
        if (choiceUser.getSelectionModel().getSelectedItem() == null) {
            showDataMissingError("User");
            return false;
        }
        return true;
    }

    private boolean timeParseValid() {
        // check date and time separately
        try {
            LocalDate.parse(dateDate.getEditor().getText(), DateTimeFormatter.ofPattern(DATE_FORMAT.getPattern()));
        } catch (Exception e) {
            showTimeParseError("Day");
            return false;
        }

        try {
            LocalTime.parse(fieldStart.getText(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern()));
        } catch (Exception e) {
            showTimeParseError("Start");
            return false;
        }

        try {
            LocalTime.parse(fieldEnd.getText(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern()));
        } catch (Exception e) {
            showTimeParseError("End");
            return false;
        }

        return true;
    }

    private boolean timeBusinessValid() {
        // only want to check time
        if (LocalTime.parse(fieldStart.getText(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern()))
                .isBefore(LocalTime.parse(BUSINESS_START.getValue(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern())))) {
            showTimeBusinessError("start");
            return false;
        }
        if (LocalTime.parse(fieldEnd.getText(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern()))
                .isAfter(LocalTime.parse(BUSINESS_END.getValue(), DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern())))) {
            showTimeBusinessError("end");
            return false;
        }
        return true;
    }

    // LAMBDA
    // Used here to filter through appointments with predicate to detect if we are overlapping
    // any appoints. As the rest, this allows us to work with the list iteratively.
    //
    // *** appointment.getUserId() == sessionService.getCurrentUser().getUserId() &&
    //
    // Can add this to also filter based on current user and appointment user. This is a
    // good business consideration as we should allow multiple users to be in appointments at once
    private boolean timeOverlapValid() {
        // compose full datetime now to check against other datetimes
        LocalDateTime start = LocalDateTime.parse(dateDate.getEditor().getText() + " " + fieldStart.getText(),
                DateTimeFormatter.ofPattern(DATE_TIME_FORMAT.getPattern()));

        if (!appointmentService.getAllAppointments().filtered(appointment ->
                (start.isAfter(appointment.getStart()) || start.isEqual(appointment.getStart())) && start.isBefore(appointment.getEnd())
        ).isEmpty()) {
            showTimeOverLapError("Start");
            return false;
        }

        LocalDateTime end = LocalDateTime.parse(dateDate.getEditor().getText() + " " + fieldEnd.getText(),
                DateTimeFormatter.ofPattern(DATE_TIME_FORMAT.getPattern()));

        if (!appointmentService.getAllAppointments().filtered(appointment ->
                end.isAfter(appointment.getStart()) && (end.isBefore(appointment.getEnd()) || end.isEqual(appointment.getEnd()))
        ).isEmpty()) {
            showTimeOverLapError("End");
            return false;
        }

        if (!appointmentService.getAllAppointments().filtered(appointment ->
                (start.isBefore(appointment.getStart()) || start.isEqual(appointment.getStart())) && (end.isAfter(appointment.getEnd()) || end.isEqual(appointment.getEnd()))
        ).isEmpty()) {
            showTimeOverLapError("Start and End surround another appointment");
            return false;
        }

        return true;
    }

    // ideally combine these all generically
    private void showDataMissingError(String badData) {
        alertService.showAlertError(translationService.translate("Common.Error.Data.Missing.Header"),
                String.format(translationService.translate("Common.Error.Data.Missing.Message"), badData));
    }

    private void showTimeParseError(String badData) {
        alertService.showAlertError(translationService.translate("Common.Error.Data.Time.Parse.Header"),
                String.format(translationService.translate("Common.Error.Data.Time.Parse.Message"), badData));
    }

    private void showTimeBusinessError(String badData) {
        alertService.showAlertError(translationService.translate("Appointment.Validation.Error.Time.Business.Header"),
                String.format(translationService.translate("Appointment.Validation.Error.Time.Business.Message"), badData));
    }

    private void showTimeOverLapError(String badData) {
        alertService.showAlertError(translationService.translate("Appointment.Validation.Error.Time.Overlap.Header"),
                String.format(translationService.translate("Appointment.Validation.Error.Time.Overlap.Message"), badData));
    }


    @Override
    public void setData(Object data) {
        if (data instanceof Appointment) {
            Appointment appointment = (Appointment) data;
            choiceCustomer.getSelectionModel().select(customerService.getCustomer(appointment.getCustomerId()));
            fieldTitle.setText(appointment.getTitle());
            fieldDescription.setText(appointment.getDescription());
            fieldLocation.setText(appointment.getLocation());
            fieldContact.setText(appointment.getContact());
            fieldUrl.setText(appointment.getUrl());
            dateDate.getEditor().setText(appointment.getStart().format(DateTimeFormatter.ofPattern(DATE_FORMAT.getPattern())));
            fieldStart.setText(appointment.getStart().format(DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern())));
            fieldEnd.setText(appointment.getEnd().format(DateTimeFormatter.ofPattern(TIME_FORMAT.getPattern())));
            fieldType.setText(appointment.getType());
            choiceUser.getSelectionModel().select(userService.getUser(appointment.getUserId()));
            isUpdate = true;
            storedId = appointment.getAppointmentId();
        }
    }

    public void setupChoiceboxes() {
        choiceCustomer.setItems(customerService.getAllCustomers());
        choiceCustomer.setConverter(new CustomerConverter<Customer>());
        /*
        // LAMBDA
        // originally this was used to code the location to the address of the customer, but I changed it to a field to
        // allow for any input instead as the sample data I was using had 'Online' as an option as well and I didn't
        // want to mess with splicing in data to the choiceBox from city+state or something
        choiceCustomer.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
                fieldLocation.setText(
                        addressService.getAddress(
                                choiceCustomer.getItems().get(
                                        newValue.intValue() >= 0 ? newValue.intValue() : 0
                                ).getAddressId()
                        ).toString()
                )
        );*/

        choiceUser.setItems(userService.getAllUsers());
        choiceUser.setConverter(new UserConverter<User>());
    }
}
