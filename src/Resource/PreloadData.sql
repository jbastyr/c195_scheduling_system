DELETE FROM appointment WHERE appointmentId IS NOT NULL;
DELETE FROM customer WHERE customerId IS NOT NULL;
DELETE FROM address WHERE addressId IS NOT NULL;
DELETE FROM city WHERE cityId IS NOT NULL;
DELETE FROM country WHERE countryId IS NOT NULL;
DELETE FROM user WHERE userId IS NOT NULL;


INSERT INTO user
	(userId, userName,password,active,createBy,createDate,lastUpdateBy) VALUES
		 (1, 'yourName','yourName',1,'admin',CURRENT_TIMESTAMP,'admin'),
		 (2, 'admin','admin',1,'admin',CURRENT_TIMESTAMP,'admin'),
		 (3, 'user','user',1,'admin',CURRENT_TIMESTAMP,'admin'),
		 (4, 'test','test',1,'admin',CURRENT_TIMESTAMP,'admin');


INSERT INTO country
	(countryId, country,createDate,createdBy,lastUpdateBy) VALUES
		(1, 'USA',CURRENT_TIMESTAMP,'admin','admin'),
		(2, 'Japan',CURRENT_TIMESTAMP,'admin','admin'),
		(3, 'Australia',CURRENT_TIMESTAMP,'admin','admin'),
		(4, 'Russia',CURRENT_TIMESTAMP,'admin','admin'),
		(5, 'Britain',CURRENT_TIMESTAMP,'admin','admin');

INSERT INTO city
	(cityId, city,countryId,createDate,createdBy,lastUpdateBy) VALUES
    (1,'Washington',1,CURRENT_TIMESTAMP,'admin','admin'),
    (2,'New York',1,CURRENT_TIMESTAMP,'admin','admin'),
    (3,'Los Angeles',1,CURRENT_TIMESTAMP,'admin','admin'),
    (4,'Chicago',1,CURRENT_TIMESTAMP,'admin','admin'),
    (5,'Houston',1,CURRENT_TIMESTAMP,'admin','admin'),
    (6,'Phoenix',1,CURRENT_TIMESTAMP,'admin','admin'),

    (7,'Tokyo',2,CURRENT_TIMESTAMP,'admin','admin'),
    (8,'Toyohashi',2,CURRENT_TIMESTAMP,'admin','admin'),
    (9,'Okazaki',2,CURRENT_TIMESTAMP,'admin','admin'),
    (10,'Toyota',2,CURRENT_TIMESTAMP,'admin','admin'),
    (11,'Akita',2,CURRENT_TIMESTAMP,'admin','admin'),

    (12,'Canberra',3,CURRENT_TIMESTAMP,'admin','admin'),
    (13,'Sydney',3,CURRENT_TIMESTAMP,'admin','admin'),
    (14,'Melbourne',3,CURRENT_TIMESTAMP,'admin','admin'),
    (15,'Brisbane',3,CURRENT_TIMESTAMP,'admin','admin'),
    (16,'Perth',3,CURRENT_TIMESTAMP,'admin','admin'),

    (17,'Moscow',4,CURRENT_TIMESTAMP,'admin','admin'),
    (18,'Saint Petersburg',4,CURRENT_TIMESTAMP,'admin','admin'),
    (19,'Yeketerinburg',4,CURRENT_TIMESTAMP,'admin','admin'),
    (20,'Kazan',4,CURRENT_TIMESTAMP,'admin','admin'),
    (21,'Novosibirsk',4,CURRENT_TIMESTAMP,'admin','admin'),

    (22,'London',5,CURRENT_TIMESTAMP,'admin','admin'),
    (23,'Birmingham',5,CURRENT_TIMESTAMP,'admin','admin'),
    (24,'Manchester',5,CURRENT_TIMESTAMP,'admin','admin'),
    (25,'Glasgow',5,CURRENT_TIMESTAMP,'admin','admin'),
    (26,'Leeds',5,CURRENT_TIMESTAMP,'admin','admin'),
    (27,'Liverpool',5,CURRENT_TIMESTAMP,'admin','admin');

INSERT INTO address
	(addressId,address, address2, cityId, postalCode, phone, createDate, createdBy, lastUpdateBy) VALUES
    (1,'001 Address Road','',1,'11111','111-222-3333',CURRENT_TIMESTAMP,'admin','admin'),
    (2,'002 Address Road','',1,'11122','123-222-2132',CURRENT_TIMESTAMP,'admin','admin'),
    (3,'003 Address Road','',3,'33333','333-232-1000',CURRENT_TIMESTAMP,'admin','admin'),
    (4,'004 Address Road','',5,'44424','232-222-3201',CURRENT_TIMESTAMP,'admin','admin'),
    (5,'005 Address Road','',8,'55523','178-321-3231',CURRENT_TIMESTAMP,'admin','admin'),
    (6,'006 Address Road','',11,'55223','809-444-0300',CURRENT_TIMESTAMP,'admin','admin'),
    (7,'007 Address Road','',12,'12323','901-223-0030',CURRENT_TIMESTAMP,'admin','admin'),
    (8,'008 Address Road','',13,'23322','555-555-2323',CURRENT_TIMESTAMP,'admin','admin'),
    (9,'009 Address Road','',17,'12323','111-222-1233',CURRENT_TIMESTAMP,'admin','admin'),
	(10,'010 Address Road','',17,'2213231','111-222-2332',CURRENT_TIMESTAMP,'admin','admin'),
    (11,'011 Address Road','',19,'32123','444-222-3232',CURRENT_TIMESTAMP,'admin','admin'),
    (12,'012 Address Road','',22,'32231','555-232-0001',CURRENT_TIMESTAMP,'admin','admin'),
    (13,'013 Address Road','',21,'12332','321-434-0123',CURRENT_TIMESTAMP,'admin','admin'),
    (14,'014 Address Road','',24,'231231','663-555-0012',CURRENT_TIMESTAMP,'admin','admin'),
    (15,'015 Address Road','',27,'02132','444-323-1111',CURRENT_TIMESTAMP,'admin','admin'),
    (16,'016 Address Road','',22,'980324','555-222-0000',CURRENT_TIMESTAMP,'admin','admin'),
    (17,'017 Address Road','',18,'909323','771-332-0045',CURRENT_TIMESTAMP,'admin','admin'),
    (18,'018 Address Road','',13,'02310','869-676-1233',CURRENT_TIMESTAMP,'admin','admin'),
	(19,'019 Address Road','',2,'90230','111-222-3332',CURRENT_TIMESTAMP,'admin','admin'),
    (20,'020 Address Road','',4,'231123','555-444-4444',CURRENT_TIMESTAMP,'admin','admin'),
    (21,'021 Address Road','',6,'04213','231-555-0030',CURRENT_TIMESTAMP,'admin','admin'),
    (22,'022 Address Road','',8,'012332','432-332-1320',CURRENT_TIMESTAMP,'admin','admin'),
    (23,'023 Address Road','',10,'0321123','231-888-0000',CURRENT_TIMESTAMP,'admin','admin'),
    (24,'024 Address Road','',12,'0213213','111-999-0000',CURRENT_TIMESTAMP,'admin','admin'),
    (25,'025 Address Road','',14,'0230','111-345-0000',CURRENT_TIMESTAMP,'admin','admin');

INSERT INTO customer
	(customerId,customerName,addressId,active,createDate,createdBy,lastUpdateBy) VALUES
    (1,'Customer-1',1,1,CURRENT_TIMESTAMP,'admin','admin'),
    (2,'Customer-2',2,1,CURRENT_TIMESTAMP,'admin','admin'),
    (3,'Customer-3',3,1,CURRENT_TIMESTAMP,'admin','admin'),
    (4,'Customer-4',4,1,CURRENT_TIMESTAMP,'admin','admin'),
    (5,'Customer-5',5,1,CURRENT_TIMESTAMP,'admin','admin'),
    (6,'Customer-6',6,1,CURRENT_TIMESTAMP,'admin','admin'),
    (7,'Customer-7',7,1,CURRENT_TIMESTAMP,'admin','admin'),
    (8,'Customer-8',8,1,CURRENT_TIMESTAMP,'admin','admin'),
    (9,'Customer-9',9,1,CURRENT_TIMESTAMP,'admin','admin'),
	(10,'Customer-10',10,1,CURRENT_TIMESTAMP,'admin','admin'),
    (11,'Customer-11',11,1,CURRENT_TIMESTAMP,'admin','admin'),
    (12,'Customer-12',12,1,CURRENT_TIMESTAMP,'admin','admin'),
    (13,'Customer-13',13,1,CURRENT_TIMESTAMP,'admin','admin'),
    (14,'Customer-14',14,1,CURRENT_TIMESTAMP,'admin','admin'),
    (15,'Customer-15',15,1,CURRENT_TIMESTAMP,'admin','admin'),
    (16,'Customer-16',16,1,CURRENT_TIMESTAMP,'admin','admin'),
    (17,'Customer-17',17,1,CURRENT_TIMESTAMP,'admin','admin'),
    (18,'Customer-18',18,1,CURRENT_TIMESTAMP,'admin','admin'),
    (19,'Customer-19',19,1,CURRENT_TIMESTAMP,'admin','admin'),
    (20,'Customer-20',20,1,CURRENT_TIMESTAMP,'admin','admin');


INSERT INTO appointment
	(appointmentId, customerId,title,description,location,contact,url,start,end,createDate,createdBy,lastUpdateBy, type, userId) VALUES
		(1,1,'Meeting','First Meeting','New York, New York','admin','','2019-05-01 09:00:00','2019-05-01 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 1),
		(2,2,'Consulting','First Consultation','New York, New York','yourName','','2019-05-02 09:00:00','2019-05-02 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(3,3,'Consulting','Follow-up','New York, New York','yourName','','2019-05-03 10:00:00','2019-05-03 11:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(4,4,'Meeting','First Meeting','New York, New York','yourName','','2019-05-04 09:00:00','2019-05-04 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 4),
		(5, 5,'Consulting','First Consultation','New York, New York','yourName','','2019-05-05 09:00:00','2019-05-05 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(6,6,'Consulting','Follow-up','New York, New York','admin','','2019-05-06 09:00:00','2019-05-06 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(7,7,'Meeting','First Meeting','New York, New York','yourName','','2019-05-07 09:00:00','2019-05-07 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 3),
		(8,8,'Consulting','First Consultation','New York, New York','yourName','','2019-05-08 09:00:00','2019-05-08 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(9,9,'Consulting','Follow-up','Online','yourName','','2019-05-09 09:00:00','2019-05-09 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(10,10,'Meeting','First Meeting','New York, New York','admin','','2019-05-10 11:00:00','2019-05-10 12:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 2),
		(11,11,'Consulting','First Consultation','New York, New York','yourName','','2019-05-11 09:00:00','2019-05-11 09:45:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(12,12,'Consulting','Follow-up','New York, New York','yourName','','2019-05-12 09:00:00','2019-05-12 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(13,20,'Meeting','First Meeting','Online','yourName','','2019-05-13 09:00:00','2019-05-13 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 1),
		(14,14,'Consulting','First Consultation','Online','yourName','','2019-05-14 09:00:00','2019-05-14 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(15,15,'Consulting','Follow-up','Online','yourName','','2019-05-15 09:00:00','2019-05-15 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(16,6,'Meeting','First Meeting','Phoenix, Arizona','user1','','2019-05-16 12:00:00','2019-05-16 13:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 4),
		(17,6,'Consulting','First Consultation','Phoenix, Arizona','user1','','2019-05-17 09:00:00','2019-05-17 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(18,18,'Consulting','Follow-up','Phoenix, Arizona','user1','','2019-05-18 09:00:00','2019-05-18 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(19,19,'Meeting','First Meeting','Phoenix, Arizona','user1','','2019-05-19 09:00:00','2019-05-19 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 3),
		(20,20,'Consulting','First Consultation','Phoenix, Arizona','user1','','2019-05-20 09:00:00','2019-05-20 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(21,2,'Consulting','Follow-up','Phoenix, Arizona','user1','','2019-05-21 09:00:00','2019-05-21 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(22,2,'Consulting','Follow-up','Online','user1','','2019-05-22 09:00:00','2019-05-22 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(23,1,'Meeting','First Meeting','New York, New York','admin','','2019-05-23 09:00:00','2019-05-23 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 3),
		(24,2,'Consulting','First Consultation','New York, New York','yourName','','2019-05-24 09:00:00','2019-05-24 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(25,3,'Consulting','Follow-up','New York, New York','yourName','','2019-05-25 10:00:00','2019-05-25 11:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(26,4,'Meeting','First Meeting','New York, New York','yourName','','2019-05-26 09:00:00','2019-05-26 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 2),
		(27,5,'Consulting','First Consultation','New York, New York','yourName','','2019-05-27 09:00:00','2019-05-27 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(28,6,'Consulting','Follow-up','New York, New York','admin','','2019-05-28 09:00:00','2019-05-28 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(29,7,'Meeting','First Meeting','New York, New York','yourName','','2019-05-29 09:00:00','2019-05-29 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 1),
		(30,8,'Consulting','First Consultation','New York, New York','yourName','','2019-05-30 09:00:00','2019-05-30 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(31,9,'Consulting','Follow-up','Online','yourName','','2019-05-31 09:00:00','2019-05-31 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(32,10,'Meeting','First Meeting','New York, New York','admin','','2019-06-01 11:00:00','2019-06-01 12:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 4),
		(33,11,'Consulting','First Consultation','New York, New York','yourName','','2019-06-02 09:00:00','2019-06-02 09:45:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(34,12,'Consulting','Follow-up','New York, New York','yourName','','2019-06-03 09:00:00','2019-06-03 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(35,20,'Meeting','First Meeting','Online','yourName','','2019-06-04 09:00:00','2019-06-04 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 3),
		(36,14,'Consulting','First Consultation','Online','yourName','','2019-06-05 09:00:00','2019-06-05 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(37,15,'Consulting','Follow-up','Online','yourName','','2019-06-06 09:00:00','2019-06-06 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 1),
		(38,6,'Meeting','First Meeting','Phoenix, Arizona','user1','','2019-06-07 12:00:00','2019-06-07 13:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 2),
		(39,6,'Consulting','First Consultation','Phoenix, Arizona','user1','','2019-06-08 09:00:00','2019-06-08 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(40,18,'Consulting','Follow-up','Phoenix, Arizona','user1','','2019-06-09 09:00:00','2019-06-09 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4),
		(41,19,'Meeting','First Meeting','Phoenix, Arizona','user1','','2019-06-10 09:00:00','2019-06-10 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Meeting', 1),
		(42,20,'Consulting','First Consultation','Phoenix, Arizona','user1','','2019-06-11 09:00:00','2019-06-11 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 2),
		(43,2,'Consulting','Follow-up','Phoenix, Arizona','user1','','2019-06-12 09:00:00','2019-06-12 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 3),
		(44,2,'Consulting','Follow-up','Online','user1','','2019-06-13 09:00:00','2019-06-13 10:00:00',CURRENT_TIMESTAMP,'admin','admin', 'Consulting', 4);