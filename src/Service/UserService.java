package Service;

import Model.User;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// would probably be better to use a different abstraction than service here
// since it is not providing the same kind of utility that scene/translation does.
// perhaps repository or something would be better, oh well
public class UserService extends DataService {
    private static UserService service = null;
    private ObservableList<User> users;

    private UserService() {
        users = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public static UserService getInstance() {
        if (service == null) {
            service = new UserService();
        }
        return service;
    }

    public User getUser(int userId) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT * FROM user WHERE userId = ?"
            );
            int index = 0;
            ps.setInt(++index, userId);
            ResultSet results = ps.executeQuery();
            User user = new User();
            if (results.next()) {
                user.mapResultSet(results);
                return user;
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }

    public ObservableList<User> getAllUsers() {
        ObservableList<User> users = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    String.format("SELECT * FROM user ORDER BY %s", "userId")
            );
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                users.add(new User().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        // reload cached copy of users while keeping observer hooks
        this.users.removeIf(item -> true);
        this.users.addAll(users);
        return this.users;
    }
}
