package Service;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

public class TableBindingService {
    private static TableBindingService service = null;

    private TableBindingService() {

    }

    public static TableBindingService getInstance() {
        if (service == null) {
            service = new TableBindingService();
        }
        return service;
    }

    public void bindColumn(TableColumn<?, ?> column, String property) {
        column.setCellValueFactory(new PropertyValueFactory<>(property));
    }
}
