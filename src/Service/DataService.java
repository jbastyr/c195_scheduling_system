package Service;

import Provider.DatabaseProvider;

abstract class DataService {
    static DatabaseProvider databaseProvider;

    DataService() {
        databaseProvider = DatabaseProvider.getInstance();
    }
}
