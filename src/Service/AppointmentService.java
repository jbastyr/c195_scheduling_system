package Service;

import Model.Appointment;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class AppointmentService extends DataService {
    private static AppointmentService service = null;
    public ObservableList<Appointment> appointments;

    private AppointmentService() {
        appointments = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public static AppointmentService getInstance() {
        if (service == null) {
            service = new AppointmentService();
        }
        return service;
    }

    public void addAppointmnet(Appointment appointment) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "INSERT INTO appointment (customerId, title, description, location, contact, url, start, end, createDate, createdBy, lastUpdate, lastUpdateBy, type, userId)" +
                            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
            );
            int index = 0;
            ps.setInt(++index, appointment.getCustomerId());
            ps.setString(++index, appointment.getTitle());
            ps.setString(++index, appointment.getDescription());
            ps.setString(++index, appointment.getLocation());
            ps.setString(++index, appointment.getContact());
            ps.setString(++index, appointment.getUrl());
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getStart()));
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getEnd()));
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getCreateDate()));
            ps.setString(++index, appointment.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getLastUpdate()));
            ps.setString(++index, appointment.getLastUpdateBy());
            ps.setString(++index, appointment.getType());
            ps.setInt(++index, appointment.getUserId());

            ps.execute();

            getAllAppointments();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAppointment(Appointment appointment) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "UPDATE appointment SET " +
                            "customerId = ?, " +
                            "title = ?, " +
                            "description = ?, " +
                            "location = ?, " +
                            "contact = ?, " +
                            "url = ?, " +
                            "start = ?, " +
                            "end = ?, " +
                            "createDate = ?, " +
                            "createdBy = ?, " +
                            "lastUpdate = ?, " +
                            "lastUpdateBy = ?," +
                            "type = ?, " +
                            "userId = ? " +
                            "WHERE appointmentId = ?"
            );
            int index = 0;
            ps.setInt(++index, appointment.getCustomerId());
            ps.setString(++index, appointment.getTitle());
            ps.setString(++index, appointment.getDescription());
            ps.setString(++index, appointment.getLocation());
            ps.setString(++index, appointment.getContact());
            ps.setString(++index, appointment.getUrl());
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getStart()));
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getEnd()));
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getCreateDate()));
            ps.setString(++index, appointment.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(appointment.getLastUpdate()));
            ps.setString(++index, appointment.getLastUpdateBy());
            ps.setString(++index, appointment.getType());
            ps.setInt(++index, appointment.getUserId());
            ps.setInt(++index, appointment.getAppointmentId());

            ps.execute();

            getAllAppointments();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAppointment(int appointmentId) throws SQLException {
        PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                "DELETE FROM appointment WHERE appointmentId = ?"
        );
        int index = 0;
        ps.setInt(++index, appointmentId);

        ps.execute();

        getAllAppointments();
    }

    public ObservableList<Appointment> getAllAppointments() {
        ObservableList<Appointment> appointments = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    String.format("SELECT * FROM appointment ORDER BY %s", "appointmentId")
            );
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                appointments.add(new Appointment().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        this.appointments.removeIf(item -> true);
        this.appointments.addAll(appointments);
        return this.appointments;
    }
}
