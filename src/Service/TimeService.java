package Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimeService {
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String TIME_PATTERN = "HH:mm:ss";
    private static TimeService service = null;

    private TimeService() {

    }

    public static TimeService getInstance() {
        if (service == null) {
            service = new TimeService();
        }
        return service;
    }

    public String getDateString(Timestamp timestamp) {
        return new SimpleDateFormat(DATE_PATTERN).format(timestamp);
    }

    public String getTimeString(Timestamp timestamp) {
        return new SimpleDateFormat(TIME_PATTERN).format(timestamp);
    }

}
