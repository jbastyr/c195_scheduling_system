package Service;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class AlertService {
    private static final String ERROR_TITLE = "Error";
    private static final String CONFIRM_TITLE = "Confirm";
    private static final String INFO_TITLE = "Info";
    private static AlertService service = null;

    private AlertService() {

    }

    public static AlertService getInstance() {
        if (service == null) {
            service = new AlertService();
        }
        return service;
    }

    public void showAlertError(String header, String content) {
        createAlert(ERROR_TITLE, header, content, AlertType.ERROR).showAndWait();
    }

    public Alert createAlert(String title, String header, String content, AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        return alert;
    }

    public Alert showAlertConfirm(String header, String content) {
        return createAlert(CONFIRM_TITLE, header, content, AlertType.CONFIRMATION);
    }

    public boolean getAlertConfirmResponse(String header, String content) {
        Optional<ButtonType> result = showAlertConfirm(header, content).showAndWait();
        return result.isPresent() && result.get() == ButtonType.OK;
    }

    public void showAlertInfo(String header, String content) {
        createAlert(INFO_TITLE, header, content, AlertType.INFORMATION).showAndWait();
    }
}
