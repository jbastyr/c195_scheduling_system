package Service;

import Model.Report.AppointmentTypesPerMonthItem;
import Model.Report.AppointmentsPerTypeItem;
import Model.Report.ConsultantScheduleItem;
import Model.Report.ReportableItem;
import Model.User;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportService extends DataService {
    private static ReportService service = null;

    private ReportService() {

    }

    public static ReportService getInstance() {
        if (service == null) {
            service = new ReportService();
        }
        return service;
    }

    // Requested report type 1
    public ObservableList<AppointmentTypesPerMonthItem> appointmentTypesPerMonth() {
        ObservableList<AppointmentTypesPerMonthItem> reportItems = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT MONTHNAME(start) AS month, COUNT(*) AS count FROM appointment GROUP BY MONTHNAME(start) ORDER BY MONTH(start)"
            );

            ResultSet results = ps.executeQuery();
            while (results.next()) {
                reportItems.add(new AppointmentTypesPerMonthItem().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        return reportItems;
    }

    // Requested report type 2
    public ObservableList<ConsultantScheduleItem> schedulePerConsultant(User user) {
        // maybe same as appointmentTypesPerMonth and use tree table??
        // not sure wtf this should look like

        ObservableList<ConsultantScheduleItem> reportItems = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT title, description, start, type FROM appointment WHERE userId = ? ORDER BY start ASC"
            );
            int index = 0;
            ps.setInt(++index, user.getUserId());

            ResultSet results = ps.executeQuery();
            while (results.next()) {
                reportItems.add(new ConsultantScheduleItem().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        return reportItems;
    }

    // Requested report type 3, my choice
    public ObservableList<AppointmentsPerTypeItem> appointmentsPerType() {
        ObservableList<AppointmentsPerTypeItem> reportItems = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT type AS type, COUNT(*) AS count FROM appointment GROUP BY type ORDER BY type"
            );
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                reportItems.add(new AppointmentsPerTypeItem().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        return reportItems;
    }

    // eek
    public TableView tableViewFromList(ObservableList<ReportableItem> items) {
        TableView<ReportableItem> table = new TableView<>();
        items.get(0).getColumns().forEach(item -> {
            TableColumn<ReportableItem, ?> tableColumn = new TableColumn<>();
            tableColumn.setText(item);
            //tableColumn.get
            table.getColumns().add(tableColumn);
            TableBindingService.getInstance().bindColumn(tableColumn, item);
        });
        table.setItems(items);
        return table;
    }
}
