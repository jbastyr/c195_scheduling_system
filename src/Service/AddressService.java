package Service;

import Model.Address;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class AddressService extends DataService {
    private static AddressService service = null;
    public ObservableList<Address> addresses;


    private AddressService() {
        addresses = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public static AddressService getInstance() {
        if (service == null) {
            service = new AddressService();
        }
        return service;
    }

    public void addAddress(Address address) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "INSERT INTO address (address, address2, cityId, postalCode, phone, createDate, createdBy, lastUpdate, lastUpdateBy) " +
                            "VALUES (?,?,?,?,?,?,?,?,?)"
            );
            int index = 0;
            ps.setString(++index, address.getAddress());
            ps.setString(++index, address.getAddress2());
            ps.setInt(++index, address.getCityId());
            ps.setString(++index, address.getPostalCode());
            ps.setString(++index, address.getPhone());
            ps.setTimestamp(++index, Timestamp.valueOf(address.getCreateDate()));
            ps.setString(++index, address.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(address.getLastUpdate()));
            ps.setString(++index, address.getLastUpdateBy());

            ps.execute();

            // refresh local store with updates
            getAllAddresses();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // few of these shouldn't change but for completeness, might as well
    public void updateAddress(Address address) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "UPDATE address SET " +
                            "address = ?, " +
                            "address2 = ?, " +
                            "cityId = ?, " +
                            "postalCode = ?, " +
                            "phone = ?, " +
                            "createDate = ?, " +
                            "createdBy = ?, " +
                            "lastUpdate = ?, " +
                            "lastUpdateBy = ? " +
                            "WHERE addressId = ?"
            );
            int index = 0;
            ps.setString(++index, address.getAddress());
            ps.setString(++index, address.getAddress2());
            ps.setInt(++index, address.getCityId());
            ps.setString(++index, address.getPostalCode());
            ps.setString(++index, address.getPhone());
            ps.setTimestamp(++index, Timestamp.valueOf(address.getCreateDate()));
            ps.setString(++index, address.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(address.getLastUpdate()));
            ps.setString(++index, address.getLastUpdateBy());
            ps.setInt(++index, address.getAddressId());
            ps.execute();

            getAllAddresses();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAddress(int addressId) throws SQLException {
        PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                "DELETE FROM address WHERE addressId = ?"
        );
        int index = 0;
        ps.setInt(++index, addressId);
        ps.execute();

        getAllAddresses();
    }

    public Address getAddress(int addressId) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT * FROM address WHERE addressId = ?");
            int index = 0;
            ps.setInt(++index, addressId);
            ResultSet results = ps.executeQuery();
            Address address = new Address();
            if (results.next()) {
                address.mapResultSet(results);
                return address;
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }

    public ObservableList<Address> getAllAddresses() {
        ObservableList<Address> addresses = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    String.format("SELECT * FROM address ORDER BY %s", "addressId"));
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                addresses.add(new Address().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        this.addresses.removeIf(item -> true);
        this.addresses.addAll(addresses);
        return this.addresses;
    }


}
