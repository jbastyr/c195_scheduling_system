package Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

public class DatabaseService extends DataService {
    private static final String RESOURCE_PATH_ROOT = "Resource";
    private static final String RESOURCE_FILE_SQL_PRELOAD = "PreloadData.sql";
    private static DatabaseService service = null;

    private DatabaseService() {

        //preloadData();
    }

    public static DatabaseService getInstance() {
        if (service == null) {
            service = new DatabaseService();
        }
        return service;
    }

    public void preloadData() {
        String preloadDataFilePath = String.format("%s/%s", RESOURCE_PATH_ROOT, RESOURCE_FILE_SQL_PRELOAD);
        InputStream dataStream = getClass().getClassLoader().getResourceAsStream(preloadDataFilePath);
        InputStreamReader dataInputStream = new InputStreamReader(dataStream);
        BufferedReader dataReader = new BufferedReader(dataInputStream);

        String[] sqlStatements = dataReader.lines().collect(Collectors.joining()).split(";");
        try {
            Statement statement = databaseProvider.getConnection().createStatement();
            for (String sql : sqlStatements) {
                statement.addBatch(sql);
            }
            statement.executeBatch();
        } catch (SQLException e) {
            //TODO:JEREMY handle this better. if we have an sql exception here there is probably something pretty wrong
            e.printStackTrace();
            System.exit(-1);
        }

    }
}