package Service;

import Model.Customer;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class CustomerService extends DataService {
    private static CustomerService service = null;
    public ObservableList<Customer> customers;

    private CustomerService() {

        customers = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public static CustomerService getInstance() {
        if (service == null) {
            service = new CustomerService();
        }
        return service;
    }

    public void addCustomer(Customer customer) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "INSERT INTO customer (customerName, addressId, active, createDate, createdBy, lastUpdate, lastUpdateBy)" +
                            "VALUES (?,?,?,?,?,?,?)"
            );
            int index = 0;
            ps.setString(++index, customer.getCustomerName());
            ps.setInt(++index, customer.getAddressId());
            ps.setInt(++index, customer.getActive());
            ps.setTimestamp(++index, Timestamp.valueOf(customer.getCreateDate()));
            ps.setString(++index, customer.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(customer.getLastUpdate()));
            ps.setString(++index, customer.getLastUpdateBy());

            ps.execute();

            getAllCustomers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCustomer(Customer customer) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "UPDATE customer SET " +
                            "customerName = ?, " +
                            "addressId = ?, " +
                            "active = ?, " +
                            "createDate = ?," +
                            "createdBy = ?, " +
                            "lastUpdate = ?, " +
                            "lastUpdateBy = ?" +
                            "WHERE customerId = ?"
            );
            int index = 0;
            ps.setString(++index, customer.getCustomerName());
            ps.setInt(++index, customer.getAddressId());
            ps.setInt(++index, customer.getActive());
            ps.setTimestamp(++index, Timestamp.valueOf(customer.getCreateDate()));
            ps.setString(++index, customer.getCreatedBy());
            ps.setTimestamp(++index, Timestamp.valueOf(customer.getLastUpdate()));
            ps.setString(++index, customer.getLastUpdateBy());
            ps.setInt(++index, customer.getCustomerId());

            ps.execute();

            getAllCustomers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCustomer(int customerId) throws SQLException {
        PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                "DELETE FROM customer WHERE customerId = ?"
        );
        int index = 0;
        ps.setInt(++index, customerId);

        ps.execute();

        getAllCustomers();
    }

    public Customer getCustomer(int customerId) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT * FROM customer WHERE customerId = ?"
            );
            int index = 0;
            ps.setInt(++index, customerId);
            ResultSet results = ps.executeQuery();
            Customer customer = new Customer();
            if (results.next()) {
                customer.mapResultSet(results);
                return customer;
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }

    // LAMBDA
    // This is a very simple use of a lambda and kind of hacky for the use case here
    // but I wanted to remove all items from the observable list and filtering through removeIf
    // with a predicate which is always true will just remove everything
    public ObservableList<Customer> getAllCustomers() {
        ObservableList<Customer> customers = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    String.format("SELECT * FROM customer ORDER BY %s", "customerId"));
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                customers.add(new Customer().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        this.customers.removeIf(item -> true);
        this.customers.addAll(customers);
        return this.customers;
    }
}
