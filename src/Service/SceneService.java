package Service;

import ViewController.DataAcceptor;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class SceneService {

    private static SceneService service = null;

    private SceneService() {
    }

    public static SceneService getInstance() {
        if (service == null) {
            service = new SceneService();
        }

        return service;
    }

    public void launchScene(Stage stage, URL launchURL) {
        try {
            Parent root = FXMLLoader.load(launchURL);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void launchScene(URL launchURL, String title, boolean modal) {
        try {
            Parent root = FXMLLoader.load(launchURL);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle(title);
            if (modal) {
                stage.initModality(Modality.APPLICATION_MODAL);
            }
            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void launchScene(URL launchURL, String title, boolean modal, Object data) {
        try {
            FXMLLoader loader = new FXMLLoader(launchURL);
            Parent root = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle(title);
            if (modal) {
                stage.initModality(Modality.APPLICATION_MODAL);
            }
            if (loader.getController() instanceof DataAcceptor) {
                ((DataAcceptor) loader.getController()).setData(data);
            }

            stage.show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void closeScene(Pane rootPane) {
        if (rootPane.getScene().getWindow() instanceof Stage) {
            ((Stage) rootPane.getScene().getWindow()).close();
        }
    }
}
