package Service;

import Model.City;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CityService extends DataService {
    private static CityService service = null;

    private CityService() {

    }

    public static CityService getInstance() {
        if (service == null) {
            service = new CityService();
        }
        return service;
    }

    public City getCity(int cityId) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT * FROM city WHERE cityId = ?");
            int index = 0;
            ps.setInt(++index, cityId);
            ResultSet results = ps.executeQuery();
            City city = new City();
            if (results.next()) {
                city.mapResultSet(results);
                return city;
            }
            return null;
        } catch (SQLException e) {
            return null;
        }
    }

    public ObservableList<City> getAllCities() {
        ObservableList<City> cities = new SimpleListProperty<>(FXCollections.observableArrayList());
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    String.format("SELECT * FROM city ORDER BY %s", "cityId"));
            ResultSet results = ps.executeQuery();
            while (results.next()) {
                cities.add(new City().mapResultSet(results));
            }
        } catch (SQLException e) {
            return null;
        }
        return cities;
    }
}
