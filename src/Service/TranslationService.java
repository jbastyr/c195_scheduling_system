package Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;

public class TranslationService {
    private static final Language DEFAULT_LANGUAGE = Language.ENGLISH;
    private static final String RESOURCE_PATH_ROOT = "Resource";
    private static TranslationService service = null;
    private static Language language = null;
    private static HashMap<String, String> translationMap;

    private TranslationService() {
        translationMap = new HashMap<>();
        setLanguage(getSystemLanguage());
        loadTranslationFile(getLanguage());
    }

    public static TranslationService getInstance() {
        if (service == null) {
            service = new TranslationService();
        }
        return service;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language lang) {
        language = lang;
    }

    // could pull the keys into enums to call but it doesnt matter for this
    public String translate(String key) {
        return translationMap.get(key);
    }

    public Language getSystemLanguage() {
        return mapLanguage(Locale.getDefault().getLanguage());
    }

    // not the best way to do this but its fine
    private Language mapLanguage(String lang) {
        switch (lang) {
            case "en":
                return Language.ENGLISH;
            case "es":
                return Language.SPANISH;
            case "default":
            default:
                return DEFAULT_LANGUAGE;
        }
    }

    private void loadTranslationFile(Language language) {
        String translationFilePath = String.format("%s/%s.translate", RESOURCE_PATH_ROOT, language.toString());
        InputStream configStream = getClass().getClassLoader().getResourceAsStream(translationFilePath);
        InputStreamReader configInputStream = new InputStreamReader(configStream);
        BufferedReader configReader = new BufferedReader(configInputStream);

        // Obviously this isn't very safe but it works for what I want here as long as config are sane
        configReader.lines().forEach(line -> {
            if (!line.isEmpty() && line.charAt(0) != '#') {
                String key, value;
                try {
                    key = line.split("=")[0];
                    value = line.split("=")[1];
                } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                    // something wrong with trans file
                    key = "";
                    value = "";
                }
                if (!key.isEmpty() && !value.isEmpty()) {
                    translationMap.put(key, value);
                }
            }
        });

    }
}
