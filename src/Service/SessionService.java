package Service;

import Model.User;

public class SessionService {
    private static SessionService service = null;
    private User user = null;

    private SessionService() {

    }

    public static SessionService getInstance() {
        if (service == null) {
            service = new SessionService();
        }
        return service;
    }

    public User getCurrentUser() {
        return user;
    }

    public void setCurrentUser(User user) {
        this.user = user;
    }

}
