package Service;

import Model.User;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static Constant.PathConstant.LOGIN_AUDIT_PATH;
import static Constant.PatternConstant.DATE_TIME_FORMAT;

public class LoginService extends DataService {

    private static LoginService service = null;

    private LoginService() {

    }

    public static LoginService getInstance() {
        if (service == null) {
            service = new LoginService();
        }
        return service;
    }

    // the actual sql calls could be abstracted to a repository or dal but it doesnt matter for this project
    public User login(String username, String password) {
        try {
            PreparedStatement ps = databaseProvider.getConnection().prepareStatement(
                    "SELECT * FROM user WHERE userName = ? AND password = ? LIMIT 1");
            int index = 0;
            ps.setString(++index, username);
            ps.setString(++index, password);
            ResultSet results = ps.executeQuery();
            User user = new User();
            if (results.next()) {
                user.mapResultSet(results);
                if (user.getUserId() != 0) {
                    logLoginEvent(username, "Sucessfully logged in");
                    return user;
                }
            }
            logLoginEvent(username, "Failed login attempt");
            return null;
        } catch (SQLException e) {
            logLoginEvent(username, "SQL Exception occurred trying to login");
            return null;
        }
    }

    // error handling isnt the best but it should work well enough for this project
    private void logLoginEvent(String username, String info) {
        OutputStream outputStream = null;
        String logEntry = String.format("[%s] Login attempted for %s, status: %s\r\n",
                LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT.getPattern())), username, info);
        boolean append = true;
        try {
            File outputFile = new File(LOGIN_AUDIT_PATH.getPath());
            outputFile.getParentFile().mkdirs();
            outputStream = new FileOutputStream(outputFile, append);
            outputStream.write(logEntry.getBytes(), 0, logEntry.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // eek
    public void showLoginAuditFile() throws IOException {
        Desktop.getDesktop().open(new File(LOGIN_AUDIT_PATH.getPath()));
    }
}
