package Service;

public enum Language {
    ENGLISH,
    SPANISH;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
