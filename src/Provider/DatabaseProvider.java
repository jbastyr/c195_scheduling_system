package Provider;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseProvider {
    private static final String databaseServer = "52.206.157.109";
    private static final String databaseName = "U059H9";
    private static final String userName = "U059H9";
    private static final String userPassword = "53688440329";
    private static DatabaseProvider provider = null;
    private static Connection connection = null;

    private DatabaseProvider() {
        connect();
    }

    public static DatabaseProvider getInstance() {
        if (provider == null) {
            provider = new DatabaseProvider();
        }
        return provider;
    }

    private void connect() {
        try {
            String url = "jdbc:mysql://" + databaseServer + "/" + databaseName;
            connection = DriverManager.getConnection(url, userName, userPassword);
        } catch (SQLException e) {
            // ehh not a good way to handle this but we need db access for everything
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            // ehh not a good way to handle this either
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }
}