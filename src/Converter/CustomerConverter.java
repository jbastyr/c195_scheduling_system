package Converter;

import Model.Customer;
import Service.CustomerService;
import javafx.util.StringConverter;

public class CustomerConverter<T> extends StringConverter<Customer> {
    public Customer fromString(String customerString) {
        return CustomerService.getInstance().getAllCustomers().filtered(customer -> customer.toString().equals(customerString)).get(0);
    }

    public String toString(Customer customer) {
        return customer.toString();
    }
}
