package Converter;

import Model.User;
import Service.UserService;
import javafx.util.StringConverter;

public class UserConverter<T> extends StringConverter<User> {
    public User fromString(String userString) {
        return UserService.getInstance().getAllUsers().filtered(user -> user.toString().equals(userString)).get(0);
    }

    public String toString(User user) {
        return user.toString();
    }
}
