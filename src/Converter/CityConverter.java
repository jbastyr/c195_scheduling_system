package Converter;

import Model.City;
import Service.CityService;
import javafx.util.StringConverter;

public class CityConverter<T> extends StringConverter<City> {
    public City fromString(String cityString) {
        return CityService.getInstance().getAllCities().filtered(city -> city.toString().equals(cityString)).get(0);
    }

    public String toString(City city) {
        return city.toString();
    }
}
