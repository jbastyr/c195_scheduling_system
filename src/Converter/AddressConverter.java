package Converter;

import Model.Address;
import Service.AddressService;
import javafx.util.StringConverter;

public class AddressConverter<T> extends StringConverter<Address> {
    public Address fromString(String addressString) {
        return AddressService.getInstance().getAllAddresses().filtered(address -> address.toString().equals(addressString)).get(0);
    }

    public String toString(Address address) {
        return address.toString();
    }
}
